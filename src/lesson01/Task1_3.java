package lesson01;

import java.util.Scanner;

public class Task1_3 {
    public static void main(String[] args) {
        System.out.print("Enter digit: ");
        Scanner scan1 = new Scanner(System.in);
        int number = scan1.nextInt();
        System.out.println("You just entered: " + number);
    }
}