package lesson06;

import test.Assert;

public class Task10_44 {
    public static void main(String[] args) {
        Assert.assertEquals(5, digRadical(4514));
    }

    static int digRadical(int a) {
        if (a / 10 == 0) {
            return a;
        } else {
            return digRadical(Task10_43.digSum(a)); //used method digSum from Task 10.43, that evaluates sum of digits
        }
    }
}