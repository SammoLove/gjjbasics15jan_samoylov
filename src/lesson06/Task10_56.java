package lesson06;

import test.Assert;

public class Task10_56 {
    public static void main(String[] args) {
        Assert.assertEquals(true, isPrimeNumber(11));
        Assert.assertEquals(false, isPrimeNumber(12));
    }

    public static boolean isPrimeNumber(int a) {
        return isPrimeNumber(a, 3, (int) Math.sqrt(a)); //calculation of the radical taken out from the cycle to optimize it
    }

    private static boolean isPrimeNumber(int a, int i, int max) {
        boolean res;
        if (a == 2 || a == 3 || a == 5 || a == 7) { //downstream filter does not work to these numbers
            return true;
        } else if (a <= 1 || a % 2 == 0 || a % 3 == 0 || a % 5 == 0 || a % 7 == 0) { //immediately filters the most popular values to accelerate further bruteforce, this is part of the Sieve of Eratosthenes alogritma
            return false;
        } else if (a % i == 0) { // and here it begins a bruteforce
            return false;
        } else if (i <= max) { // enumeration of divisors goes not to the number itself, but to its radical (max) to increase the enumeration speed, because after the radical divisors repeated a multiple
            res = isPrimeNumber(a, (i + 2), max); //if not divisible, try the following an ODD divider, another optimization
        } else {
            res = true; //all filters have been passed, all divisors tried, but did not find anyone, so returns yes, it is a simple prime number
        }
        return res;
    }
}