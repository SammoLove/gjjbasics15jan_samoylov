package lesson06;

import test.Assert;

public class Task10_52 {
    public static void main(String[] args) {
        Assert.assertEquals(97531, digReverse(13579));
    }

    //this overloaded method is for hiding of unnecessary for user, but needed to work field
    public static int digReverse(int a) {
        return digReverse(a, 0);
    }

    private static int digReverse(int a, int out) {
        //there is possible one line implementation, but there are conventions "don't use 'everything in one line' code style".
        //return a / 10 == 0 ? out * 10 + a : digReverse(a / 10, out * 10 + a % 10);

        if (a / 10 == 0) {
            return out * 10 + a;
        } else {
            return digReverse(a / 10, out * 10 + a % 10);
        }
    }
}