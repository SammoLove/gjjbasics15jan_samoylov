package lesson06;

import test.Assert;

public class Task10_41 {
    public static void main(String[] args) {
        int n = 4;
        Assert.assertEquals(24, fact(n));
    }

    static int fact(int n) {
        if (n == 1) {
            return 1;
        }
        return fact(n - 1) * n;
    }
}