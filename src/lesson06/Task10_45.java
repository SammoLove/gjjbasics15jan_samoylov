package lesson06;

import test.Assert;

public class Task10_45 {
    public static void main(String[] args) {
        int start = 3;
        int diff = 2;
        int n = 4;
        Assert.assertEquals(9, getNMember(start, diff, n));
        Assert.assertEquals(3 + 5 + 7 + 9, sumNMembers(start, diff, n));
    }

    static int getNMember(int start, int diff, int n) {
        if (n == 1) {
            return start;
        } else {
            return getNMember(start + diff, diff, n - 1);
        }
    }

    static int sumNMembers(int start, int diff, int n) {
        if (n == 1) {
            return start;
        } else {
            return sumNMembers(start + diff, diff, n - 1) + start;
        }
    }
}