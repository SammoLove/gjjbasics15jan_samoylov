package lesson06;

import test.Assert;

import java.lang.reflect.Array;

public class Task10_53 {
    public static void main(String[] args) {
        int[] inArr = {2, 24, 12, 9, 0};
        int[] reversedArr = {9, 12, 24, 2};
        Assert.assertEquals(reversedArr, numReverse(inArr));
    }

    //this overloaded method is for hiding of unnecessary for user, but needed to work field
    public static int[] numReverse(int[] arr) {
        int[] revArr = (int[]) Array.newInstance(arr.getClass().getComponentType(), arr.length - 1);
        return numReverse(arr, 0, revArr.length - 1, revArr);
    }

    private static int[] numReverse(int[] arr, int i, int j, int[] revArr) {
        if (arr[i] > 0) {
            revArr[j] = arr[i];
            numReverse(arr, i + 1, j - 1, revArr);
        }
        return revArr;
    }
}