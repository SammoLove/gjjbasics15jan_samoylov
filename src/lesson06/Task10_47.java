package lesson06;

import test.Assert;

public class Task10_47 {
    public static void main(String[] args) {
        Assert.assertEquals(8, getNFibonachiMember(6));
    }

    static int getNFibonachiMember(int n) {
        if (n == 1 | n == 2) {
            return 1;
        } else {
            return getNFibonachiMember(n - 1) + getNFibonachiMember(n - 2);
        }
    }
}