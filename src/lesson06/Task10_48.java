package lesson06;

import test.Assert;

public class Task10_48 {
    public static void main(String[] args) {
        int[] arr = {-61, 2, -15, 90, 89, 44};
        Assert.assertEquals(90, getMaxOfArr(arr));
    }

    //this overloaded method is for hiding of two unnecessary for user, but needed to work fields
    public static int getMaxOfArr(int[] arr) {
        return getMaxOfArr(arr, arr.length - 1, arr[arr.length - 1]);
    }

    private static int getMaxOfArr(int[] arr, int pos, int max) {
        pos--;
        if (pos < 0) {
            return max;
        }
        return arr[pos] > max ? getMaxOfArr(arr, pos, arr[pos]) : getMaxOfArr(arr, pos, max);
    }
}