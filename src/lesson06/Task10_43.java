package lesson06;

import test.Assert;

public class Task10_43 {
    public static void main(String[] args) {
        Assert.assertEquals(9, digSum(3123));
        Assert.assertEquals(4, digAmount(3123));
    }

    static int digSum(int a) {
        if (a / 10 == 0) {
            return a;
        } else {
            int tmp = a % 10;
            a = a / 10;
            return tmp + digSum(a);
        }
    }

    static int digAmount(int a) {
        if (a / 10 == 0) {
            return 1;
        } else {
            a = a / 10;
            return 1 + digAmount(a);
        }
    }
}