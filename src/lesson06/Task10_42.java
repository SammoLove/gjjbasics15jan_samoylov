package lesson06;

import test.Assert;

public class Task10_42 {
    public static void main(String[] args) {
        Assert.assertEquals(16, toDegrees(2, 4));
    }

    static int toDegrees(int a, int n) {
        int res;
        if (n == 0) {
            return 1;
        } else if (n == 1) {
            return a;
        } else {
            res = a * toDegrees(a, n - 1);
        }
        return res;
    }
}