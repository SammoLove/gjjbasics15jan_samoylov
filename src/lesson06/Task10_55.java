package lesson06;

import test.Assert;

public class Task10_55 {
    public static void main(String[] args) {
        Assert.assertEquals("CAFE", convertTo(51966, (byte) 16));
    }

    public static String convertTo(int num, byte numSystem) {
        final char[] signs = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        String sOst = "";
        int dOst = num % numSystem;
        num /= numSystem;
        if (num != 0) {
            sOst += convertTo(num, numSystem) + signs[dOst];
        } else {
            sOst += signs[dOst];
            return sOst;
        }
        return sOst;
    }
}