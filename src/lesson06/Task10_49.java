package lesson06;

import test.Assert;

public class Task10_49 {
    public static void main(String[] args) {
        int[] arr = {-61, 2, -15, 90, 89, 44};
        Assert.assertEquals(3, getMaxIndexOfArr(arr));
    }

    //this overloaded method is for hiding of three unnecessary for user, but needed to work fields
    public static int getMaxIndexOfArr(int[] arr) {
        int lng = arr.length;
        return getMaxIndexOfArr(arr, lng - 1, arr[lng - 1], lng - 1);
    }

    private static int getMaxIndexOfArr(int[] arr, int pos, int max, int maxPos) {
        pos--;
        if (pos < 0) {
            return maxPos;
        }
        return arr[pos] > max ? getMaxIndexOfArr(arr, pos, arr[pos], pos) : getMaxIndexOfArr(arr, pos, max, maxPos);
    }
}