package lesson06;

import test.Assert;

public class Task10_50 {
    public static void main(String[] args) {
        Assert.assertEquals(5, funcAcerman(1, 3));
        //right answer "5" from table of values in wikipedia article
    }

    static int funcAcerman(int n, int m) {
        if (n == 0) {
            return m + 1;
        }
        if (n > 0) {
            if (m == 0) {
                return funcAcerman(n - 1, 1);
            } else if (m > 0) {
                return funcAcerman(n - 1, funcAcerman(n, m - 1));
            }
        }
        return 0;
    }
}