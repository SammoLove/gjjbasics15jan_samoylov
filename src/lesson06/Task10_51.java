package lesson06;

public class Task10_51 {
    public static void main(String[] args) {
        procA(5);
        System.out.println();
        procB(5);
        System.out.println();
        procC(5);
    }

    /**
     * The procedure displays 5 for the first time
     * Then calls itself with a value of 5-1, and again displays the first four, then calls itself
     * When n goes to 1, 1 is displayed,
     * In the next iteration, for n = 0 the procedure becomes nothing to execute, and it ends *
     */
    static void procA(int n) {
        if (n > 0) {
            System.out.println(n);
            procA(n - 1);
        }
    }

    /**
     * The procedure which was called from "main" with a value of n=5 calls itself with a value of 5~1,
     * Then it calls itself again and again and not reaching the end of the "if" block execution.
     * And so it is until for n=0 will not "stumble" on the condition "n>0",
     * So, the procedure completes, and returns to the place, where n was equal to 1.
     * Since the procedure ended, the next step will execute, printing n = 1
     * Nested procedure completes again and will release to the next level,
     * where the procedure completed for n=0, and so on *
     */
    static void procB(int n) {
        if (n > 0) {
            procB(n - 1);
            System.out.println(n);
        }
    }

    /**
     * In this procedure, first, everything happens exactly as in the first -
     * - the procedure deepens inside itself before the end is reaching.
     * In the last step, when n=1 prints 1 and the procedure is calls for n=0.
     * For n=0 "if" block is not executed, and the first call goes output in end of the procedure.
     * Next further returns, "if" block has already been executed there, and again goes output at the end of the procedure.
     * Since n on the earlier steps used to be larger, the output goes in reverse order. *
     */
    static void procC(int n) {
        if (n > 0) {
            System.out.println(n);
            procC(n - 1);
        }
        System.out.println(n);
    }
}