package lesson09;

import test.Assert;

public class Task1 {
    public static void main(String[] args) {
        AssociativeArray contacts = new AssociativeArray(4);
        contacts.add(222, "Поросёнок Пётр");
        contacts.add(333, "Иванов Пётр");
        contacts.add(444, "Петров Иван");

        Assert.assertEquals("Иванов Пётр", contacts.get(333));
        contacts.remove(111);
        contacts.remove(333);
        Assert.assertEquals(null, contacts.get(333));

        contacts.add(555, "Пятый");
        contacts.add(666, "Шестой");
        contacts.add(777, "Седьмой");
        contacts.add(888, "Восьмой");

        contacts.add(100, "Десятый");
        contacts.add(888, "Новый Восьмой");
        Assert.assertEquals("Новый Восьмой", contacts.get(888));

        contacts.remove(666);
        Assert.assertEquals(null, contacts.get(666));

        contacts.add("polygenelubricants", "polygenelubricants");
        contacts.add("GydZG_", "GydZG_");
        contacts.add("DESIGNING WORKHOUSES", "DESIGNING WORKHOUSES");
        //JDK hash of this string is max integer value

        Assert.assertEquals("Седьмой", contacts.get(777));
        Assert.assertEquals("GydZG_", contacts.get("GydZG_"));
    }
}