package lesson09;

public class AssociativeArray {
    private Cell[] array;
    private int capacity; // redundant var ?
    private int filled; //size?

    public AssociativeArray(int capacity) {
        this.capacity = capacity;
        array = new Cell[capacity];
    }

    public AssociativeArray() {
        this(10);
    }

    private void rehash(int newCapacity) {
        Cell[] newArray = new Cell[newCapacity];
        capacity = newCapacity;
        for (Cell i : array) {
            if (i != null) {
                if (i.hasNext()) { //���� � ������ ���� ������ ���� �������
                    Object currKey = i.getKey();
                    int newHash = getHash(currKey);
                    if (newArray[newHash] == null) {
                        newArray[newHash] = i;
                    } else {
                        newArray[newHash].setData(currKey, i.getData(currKey));
                    }
                } else {
                    do {
                        int newHash = getHash(i.getKey());
                        if (newArray[newHash] == null) {
                            newArray[newHash] = i;
                        } else {
                            newArray[newHash].setData(i.getKey(), i.getData(i.getKey()));
                        }
                        i = i.getNext();
                    } while (i != null);
                }
            }
        }
        array = newArray;
    }

    private void checkCapacity() {
        int maxFilledCapacity = (int) (0.4 * capacity);
        int increaseMultiplier = 4; //0.4 and 4  - experimentally determined values
        if (filled > maxFilledCapacity) {
            rehash(increaseMultiplier * capacity);
        }
    }

    public void add(Object key, Object data) {
        checkCapacity();
        int hash = getHash(key);
        if (array[hash] == null) {
            array[hash] = new Cell(key, data);
        } else {
            array[hash].setData(key, data);
        }
        filled++;
    }

    public Object get(Object key) {
        return array[getHash(key)] == null ? null : array[getHash(key)].getData(key);
    }

    public Object remove(Object key) {
        filled--;
        int hash = getHash(key);
        if (array[hash] == null) {
            return null;
        } else if (!array[hash].hasNext()) {
            Object tmp = array[hash].getData(key);
            array[hash] = null;
            return tmp;
        } else {
            return array[hash].eraseData(key);
        }
    }

    private int getHash(Object key) {
        int hash = key.hashCode() % capacity;
        return hash < 0 ? -hash : hash;
    }


    private class Cell {
        private Object key;
        private Object data;
        private Cell next;

        public Cell(Object key, Object data) {
            this.key = key;
            this.data = data;
        }

        public Object getData(Object key) {
            Cell current = this;
            do {
                if (current.key.equals(key)) {
                    return current.data;
                }
                if (current.getNext() != null) {
                    current = current.getNext();
                } else {
                    break;
                }
            } while (true);
            return null;
        }

        public Object setData(Object key, Object data) {
            Cell current = this;
            do {
                if (current.key.equals(key)) { //�� �� ����� ������� ��� ���� � ����������� �������. ���� ��� ���������� ���� ��������������, ��� � �� ��� ���� ������ ���� � ����� �� ������, �� �������� ������������ ���� �����������, � ������ �������� ������������.
                    Object was = current.data; //saving of old value
                    current.data = data;
                    return was;
                } else if (current.getNext() != null) {
                    current = current.getNext();
                } else {
                    break;
                }
            } while (true);
            current.next = new Cell(key, data);
            return null;
        }

        public Object eraseData(Object keyToErase) {
            if (this.key.equals(keyToErase)) {
                Object tmp = data;
                key = this.getNext().key;
                data = this.getNext().data;
                next = (this.getNext().getNext() == null ? null : this.getNext().getNext());
                return tmp;
            }
            Cell current = this;
            while (current.getNext() != null && !(current.getNext().key.equals(keyToErase))) {
                current = current.getNext();
            }
            if (current.getNext() == null) {
                return null;
            } else if (current.getNext().getNext() == null) {
                Object tmp = current.getNext().data;
                current.next = null;
                return tmp;
            } else {
                Object tmp = current.getNext().data;
                current.next = current.getNext().getNext();
                return tmp;
            }
        }

        public boolean hasNext() {
            return this.getNext() != null;
        }

        public Cell getNext() {
            return next;
        }

        public Object getKey() {
            return key;
        }
    }
}