package lesson09;

import test.MyTimer;

import java.util.HashMap;
import java.util.Random;

//import static test.MyTimer.*;

public class AssociativeArrayTest {
    public static void main(String[] args) {
        int limon = 1000_000;
        int[] randArr = new int[limon];
        MyTimer myTimer = new MyTimer();
        Random r = new Random();
        int retries = 2;
        HashMap jdkHM = null;
        AssociativeArray myAR = null;

        for (int i = 0; i < limon; i++) {
            randArr[i] = r.nextInt(limon);
        }

        System.out.println("Test 1. Adding of 1'000'000 random elements to HashMap.");
        System.out.print("JDK HashMap. ");
        for (int j = 1; j <= retries; j++) {
            jdkHM = new HashMap();
            myTimer.start();
            for (int i : randArr) {
                jdkHM.put(i, i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        System.out.print("My AssociativeArray. ");
        for (int j = 1; j <= retries; j++) {
            myAR = new AssociativeArray();
            myTimer.start();
            for (int i : randArr) {
                myAR.add(i, i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();
        System.out.println();

        //================================

        System.out.println("Test 2. Getting of 1'000'000 random elements from HashMap.");
        System.out.print("JDK HashMap. ");
        for (int j = 1; j <= retries; j++) {
            myTimer.start();
            for (int i : randArr) {
                jdkHM.get(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        System.out.print("My AssociativeArray. ");
        for (int j = 1; j <= retries; j++) {
            myTimer.start();
            for (int i : randArr) {
                myAR.get(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();
        System.out.println();

        //================================

        limon = (limon >> 1);
        System.out.println("Test 3. Removing of half elements from HashMap.");
        System.out.print("JDK HashMap. ");
        for (int j = 1; j <= retries; j++) {
            for (int i = 0; i < limon; i++) {
                randArr[i] = r.nextInt(limon * 2);
            }
            myTimer.start();
            for (int i : randArr) {
                jdkHM.remove(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        System.out.print("My AssociativeArray. ");
        for (int j = 1; j <= retries; j++) {
            for (int i = 0; i < limon; i++) {
                randArr[i] = r.nextInt(limon * 2);
            }
            myTimer.start();
            for (int i : randArr) {
                myAR.remove(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
    }
}