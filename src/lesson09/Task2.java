package lesson09;

import test.Assert;

public class Task2 {
    public static void main(String[] args) {
        AssociativeArray bigArr = new AssociativeArray(); //dimensionless, no matter what size will be

        // key - i.j
        bigArr.add(73.63, 22);
        bigArr.add(0.99, 33);
        bigArr.add(99.0, 44);

        Assert.assertEquals(44, bigArr.get(99.0));
        Assert.assertEquals(33, bigArr.get(0.99));
        Assert.assertEquals(22, bigArr.get(73.63));

        bigArr.remove(0.99);
        Assert.assertEquals(null, bigArr.get(0.99));
    }
}