package lesson03;

import test.Assert;

public class Task1 {
    public static void main(String[] args) {

        //universal var-s
        double x = 0;
        double res;
        int a = 2, b = 3, c = 4;

        //Task 1.17 O
        res = Math.sqrt(1 - Math.pow(Math.sin(x), 2));
        System.out.println("Task 1.17 О. Result = " + res);
        Assert.assertEquals(1.0, res);

        //Task 1.17 P
        res = 1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c);
        System.out.println("Task 1.17 P. Result = " + res);
        Assert.assertEquals(.5, res);

        //Task 1.17 R
        x = 1;
        res = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
        System.out.println("Task 1.17 R. Result = " + res);
        Assert.assertEquals(0.7071067811865476, res);

        //Task 1.17 S
        res = Math.abs(x) + Math.abs(x + 1);
        System.out.println("Task 1.17 S. Result = " + res);
        Assert.assertEquals(3., res);
    }
}