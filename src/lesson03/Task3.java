package lesson03;

import test.Assert;

public class Task3 {
    public static void main(String[] args) {

        //task 3.26 a with "do while" using
        boolean x = false;
        boolean y = false;
        boolean z = false;
        String res = "";
        System.out.println("Task 3.26 a:");
        do {
            do {
                do {
                    //input
                    String xs = x ? "1" : "0";
                    String ys = y ? "1" : "0";
                    String zs = z ? "1" : "0";
                    System.out.print(xs + ys + zs);
                    //output
                    String rs = !(x | y) & (!x | !z) ? "1" : "0";
                    System.out.println(" " + rs);
                    res += rs;
                    z = !z;
                } while (z);
                y = !y;
            } while (y);
            x = !x;
        } while (x);
        Assert.assertEquals("11000000", res);


        //task 3.26 b with "for" using
        res = "";
        System.out.println("Task 3.26 b:");
        do {
            do {
                do {
                    //input
                    String xs = x ? "1" : "0";
                    String ys = y ? "1" : "0";
                    String zs = z ? "1" : "0";
                    System.out.print(xs + ys + zs);
                    //output
                    String rs = !(!x & y) | (x & !z) ? "1" : "0";
                    System.out.println(" " + rs);
                    res += rs;
                    z = !z;
                } while (z);
                y = !y;
            } while (y);
            x = !x;
        } while (x);
        Assert.assertEquals("11001111", res);


        //task 3.26 c with "while" using
        x = false;
        y = false;
        z = false;
        res = "";
        System.out.println("Task 3.26 c:");
        do {
            do {
                do {
                    //input
                    String xs = x ? "1" : "0";
                    String ys = y ? "1" : "0";
                    String zs = z ? "1" : "0";
                    System.out.print(xs + ys + zs);
                    //output
                    String rs = x | !y & !(x | !z) ? "1" : "0";
                    System.out.println(" " + rs);
                    res += rs;
                    z = !z;
                } while (z);
                y = !y;
            } while (y);
            x = !x;
        } while (x);
        Assert.assertEquals("01001111", res);


        // task 3.29
        byte dx = 25, dy = -18, dz = 111;
        System.out.print("Task 3.29a. ");
        boolean resA = (dx % 2) != 0 & (dy % 2) != 0;
        System.out.println(resA);
        Assert.assertEquals(false, resA);

        System.out.print("Task 3.29b. ");
        resA = (dx < 20) ^ (dy < 20);
        System.out.println(resA);
        Assert.assertEquals(true, resA);

        System.out.print("Task 3.29c. ");
        resA = (dx == 0) | (dy == 0);
        System.out.println(resA);
        Assert.assertEquals(false, resA);

        System.out.print("Task 3.29d. ");
        resA = (dx < 0) & (dy < 0) & (dz < 0);
        System.out.println(resA);
        Assert.assertEquals(false, resA);

        System.out.print("Task 3.29e. ");
        resA = (dx % 5 == 0) ^ (dy % 5 == 0) ^ (dz % 5 == 0);
        System.out.println(resA);
        Assert.assertEquals(true, resA);

        System.out.print("Task 3.29f. ");
        resA = (dx > 100) | (dy > 100) | (dz > 100);
        System.out.println(resA);
        Assert.assertEquals(true, resA);
    }
}