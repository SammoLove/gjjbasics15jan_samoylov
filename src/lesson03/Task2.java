package lesson03;

import test.Assert;

public class Task2 {
    public static void main(String[] args) {

        //Task 2.13
        int a = 123;
        double res = 0;
        for (int i = 2; i >= 0; i--) {
            int rem = a % 10;
            a = a / 10;
            res += rem * Math.pow(10, i);
        }
        System.out.println("Task 2.13. Result = " + res);
        Assert.assertEquals(321, res);

        //SECOND VARIANT 2.13 (without cycles)
        a = 123; //100 <= a <= 999
        res = a / 100 + a / 10 % 10 * 10 + a % 10 * 100;
        System.out.println("Task 2.13 (ver. 2). Result = " + res);
        Assert.assertEquals(321, res);

        //Task 2.31
        int x = 123; // 100 <= x <= 999
        res = x / 100 * 100 + x % 10 * 10 + ((x / 10) % 10);
        System.out.println("Task 2.31. x = " + x + ", Result = " + res);
        Assert.assertEquals(132, res);

        //Task 2.39
        float h = 5; // 0 <= h <= 23
        float m = 56; // 0 <= m <= 59
        float s = 45; // 0 <= s <= 59
        float degrees = 360 * 2 * (h / 24 + m / (24 * 60) + s / (24 * 60 * 60));
        System.out.println("Task 2.39. Hour hand held " + degrees + " degrees");
        Assert.assertEquals(178.375, degrees);

        //Task 2.43
        int divYes;
        a = 4;
        int b = 2;
        divYes = (a % b == 0) | (b % a == 0) ? 1 : 0;
        System.out.println("Task 2.43. Result = " + divYes);
        Assert.assertEquals(1, divYes);
    }
}