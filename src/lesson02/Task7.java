package lesson02;

import test.Assert;

public class Task7 {
    public static void main(String[] args) {
        int a = 2;
        int b = 5;

        task7_1(a, b);
        task7_2(a, b);
        task7_3(a, b);
        task7_4(a, b);
    }

    private static void task7_1(int a, int b) {
        int aWas = a;
        int bWas = b;
        a = a * b;
        b = a / b;
        a = a / b;

        System.out.print("1 variant... ");
        Assert.assertEquals(a, aWas, b, bWas);
    }

    private static void task7_2(int a, int b) {
        int aWas = a;
        int bWas = b;
        a += b;
        b = a - b;
        a -= b;

        System.out.print("2 variant... ");
        Assert.assertEquals(a, aWas, b, bWas);
    }

    private static void task7_3(int a, int b) {
        int aWas = a;
        int bWas = b;
        a = a ^ b;
        b = b ^ a;
        a = a ^ b;

        System.out.print("3 variant... ");
        Assert.assertEquals(a, aWas, b, bWas);
    }

    private static void task7_4(int a, int b) {
        int aWas = a;
        int bWas = b;

        a = (a & ~b) | (~a & b);
        b = (a & ~b) | (~a & b);
        a = (a & ~b) | (~a & b);

        System.out.print("4 variant... ");
        Assert.assertEquals(a, aWas, b, bWas);
    }
}