package lesson02;

import test.Assert;

public class Task6 {
    public static void main(String[] args) {
        byte n = 4;
        byte m = 3;
        Assert.assertEquals(0b1_0000, task6a(n));
        Assert.assertEquals(0b1_1000, task6b(n, m));

        int a = 0b11_0111;
        int i = 3;
        Assert.assertEquals(0b11_0000, task6c(a, i));
        Assert.assertEquals(0b11_1111, task6d(a, i));
        Assert.assertEquals(0b11_1111, task6e(a, i));
        Assert.assertEquals(0b11_0111, task6f(a, i));
        Assert.assertEquals(0b111, task6g(a, n));
        Assert.assertEquals(0b0, task6h(a, i));

        byte num = 36;
        Assert.assertEquals("00100100", task6i(num));
    }


    /**
     * Method for calculating of Tast 6A
     *
     * @param n input value (n<32)
     * @return task result
     */
    private static int task6a(byte n) {
        int res = 1 << n;
        System.out.println("A) 2^n = " + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }


    /**
     * Method for calculating of Tast 6B
     *
     * @param n input value (n<32)
     * @param m input value (n<32)
     * @return task result
     */
    private static int task6b(byte n, byte m) {
        int res = (1 << n) ^ (1 << m);
        System.out.println("B) 2^n+2^m = " + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }


    /**
     * Method for calculating of Tast 6C
     *
     * @param a input value (integer)
     * @param i input value (natural number, >0)
     * @return task result
     */
    private static int task6c(int a, int i) {
        //int res = a & (0 - (1 << i));
        int res = a & (~0 << i);
        System.out.println("C) a=" + a + "=0b" + Integer.toBinaryString(a) + ", i=" + i + ", Rusult=" + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }

    /**
     * Method for calculating of Tast 6D
     *
     * @param a input value (integer)
     * @param i input value (natural number, >0)
     * @return task result
     */
    private static int task6d(int a, int i) {
        int res = a | (1 << i);
        System.out.println("D) a=" + a + "=0b" + Integer.toBinaryString(a) + ", i=" + i + ", Rusult=" + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }


    /**
     * Method for calculating of Tast 6E
     *
     * @param a input value (integer)
     * @param i input value (natural number, >0)
     * @return task result
     */
    private static int task6e(int a, int i) {
        int res = a ^ (1 << i);
        System.out.println("E) a=" + a + "=0b" + Integer.toBinaryString(a) + ", i=" + i + ", Rusult=" + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }


    /**
     * Method for calculating of Tast 6F
     *
     * @param a input value (integer)
     * @param i input value (natural number, >0)
     * @return task result
     */
    private static int task6f(int a, int i) {
        int res = a & (~(1 << i));
        System.out.println("F) a=" + a + "=0b" + Integer.toBinaryString(a) + ", i=" + i + ", Rusult=" + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }


    /**
     * Method for calculating of Tast 6G
     *
     * @param a input value (integer)
     * @param n input value (natural number, >0)
     * @return task result
     */
    private static int task6g(int a, int n) {
        //int res = a & ((1 << n) - 1);
        int mask = ~(~0 << n);
        System.out.println("mask=" + mask + "=0b " + Integer.toBinaryString(mask));
        int res = a & ~(~0 << n);
        System.out.println("G) a=" + a + "=0b" + Integer.toBinaryString(a) + ", n=" + n + ", Rusult=" + res + "=0b" + Integer.toBinaryString(res));
        return res;
    }


    /**
     * Method for calculating of Tast 6H
     *
     * @param a input value (integer)
     * @param i input value (natural number, >0)
     * @return task result
     */
    private static int task6h(int a, int i) {
        int res;
        int mask = 1 << i;
        System.out.println("mask=" + mask + "=0b " + Integer.toBinaryString(mask));
        if ((a & mask) == mask) {
            res = 1;
        } else {
            res = 0;
        }
        System.out.println("H) a=" + a + "=0b" + Integer.toBinaryString(a) + ", i=" + i + ", Rusult=" + res);
        return res;
    }


    /** Old code
     private static String task6i(byte num) {
     int m_int = num;
     StringBuffer chkStr = new StringBuffer("00000000");
     for (int i = 0; i < 7; i++) {
     int rem = num % 2;
     num = (byte)num / 2;
     if (rem == 1) {
     chkStr.setCharAt(7 - i, '1');
     }
     }
     System.out.println("I) num (byte)=" + num + "=0b" + chkStr);
     return chkStr.toString();
     }
     */

    /**
     * Method for calculating of Tast 6I (v.2)
     *
     * @param num byte value
     * @return task result (String)
     */
    private static String task6i(byte num) {
        String str = "";
        for (byte i = 7; i >= 0; i--) {
            int mask = 1 << i;
            str += ((num & mask) == mask) ? '1' : '0';
        }
        return str;
    }
}