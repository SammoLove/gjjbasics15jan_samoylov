package lesson07;

import test.Assert;

public class Task13_11 {
    public static void main(String[] args) {
        int numberOfEmployees = 4;
        String[][] firmDB = {
                {"Петрова", "Наталья", "Ивановна", "12", "2013"},
                {"Прутков", "Козьма", "", "10", "2012"}};

        Employee[] firm = new Employee[numberOfEmployees];

        //input method 1 (with constuct with no parameters and setters)
        firm[0] = new Employee();
        firm[0].setSurname("Генеральнов");
        firm[0].setName("Директор");
        firm[0].setMiddleName("Начальникович");
        firm[0].setMonthStarted(1);
        firm[0].setYearStarted(2008);

        //input method 2 (by entering parameters of constructor and without Middle Name)
        firm[1] = new Employee("Иванов", "Вячеслав", 9, 2009);

        //input method 3 (in cycle from "DB")
        for (int i = 0; i < firmDB.length; i++) {
            firm[i + 2] = new Employee(firmDB[i][0], firmDB[i][1], firmDB[i][2], Integer.valueOf(firmDB[i][3]), Integer.valueOf(firmDB[i][4]));
        }

        //======================================
        //output for all with experience showing
        System.out.println("Список работкников и их трудовой стаж:");
        for (int i = 0; i < numberOfEmployees; i++) {
            System.out.println(i + ". " + firm[i].getName() + " " + firm[i].getSurname() + " " + firm[i].getMiddleName() + " работает " + firm[i].experienceYearsTill(10, 2014) + " полных лет");
        }
        Assert.assertEquals(2, firm[3].experienceYearsTill(2, 2015));

        //output for search results
        System.out.println();
        Searcher.searchStr(firm, "ИваН");
    }
}