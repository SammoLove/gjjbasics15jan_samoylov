package lesson07;

public class Employee {
    private String surname;
    private String name;
    private String middleName;
    private int monthStarted;
    private int yearStarted;

    public Employee(String surname, String name, String middleName, int monthStarted, int yearStarted) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.monthStarted = monthStarted;
        this.yearStarted = yearStarted;
    }

    public Employee(String surname, String name, int monthStarted, int yearStarted) {
        this(surname, name, "", monthStarted, yearStarted);
    }

    public Employee() {
        //this("", "", "", -1, -1); //was so
        this.surname = "";
        this.name = "";
        this.middleName = "";
        this.monthStarted = -1;
        this.yearStarted = -1;
    }

    public int experienceYearsTill(int month, int year) {
        if (month >= monthStarted) {
            return year - yearStarted;
        } else {
            return year - yearStarted - 1;
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getMonthStarted() {
        return monthStarted;
    }

    public void setMonthStarted(int monthStarted) {
        this.monthStarted = monthStarted;
    }

    public int getYearStarted() {
        return yearStarted;
    }

    public void setYearStarted(int yearStarted) {
        this.yearStarted = yearStarted;
    }
}
