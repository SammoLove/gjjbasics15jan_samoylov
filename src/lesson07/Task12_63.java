package lesson07;

import test.Assert;

public class Task12_63 {
    public static void main(String[] args) {
        int[][] pupils = {{20, 30, 20, 30}, {25, 35, 32, 28}, {20, 18, 22, 20}, {23, 24, 23, 23}};
        int[] target = {25, 30, 20, 23};
        Assert.assertEquals(target, parallelMed(pupils));
    }

    static int[] parallelMed(int[][] data) {
        int classes = data.length;
        int parallels = data[0].length;
        int[] res = new int[classes];

        for (int i = 0; i < classes; i++) {
            for (int j = 0; j < parallels; j++) {
                res[i] += data[i][j];
            }
            res[i] /= parallels;
        }
        return res;
    }
}