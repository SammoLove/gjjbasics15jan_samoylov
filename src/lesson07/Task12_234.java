package lesson07;

import test.Assert;

public class Task12_234 {
    public static void main(String[] args) {
        int kLine = 4;
        int sCol = 2;
        int[][] input = {
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4}};
        int[][] output = {
                {1, 3, 4, 0},
                {1, 3, 4, 0},
                {1, 3, 4, 0},
                {1, 3, 4, 0},
                {0, 0, 0, 0}};
        Assert.assertEquals(output, delStrCol(input, kLine, sCol));
    }

    public static int[][] delStrCol(int[][] arr, int k, int s) {
        int lines = arr.length;
        int cols = arr[0].length;
        int[][] arrIn = arr.clone();

        for (int i = 0; i < lines; i++) { //this cycle clears needed col and line
            for (int j = 0; j < cols; j++) {
                if ((i == k - 1) | (j == s - 1))
                    arrIn[i][j] = 0;
            }
        }
        for (int i = 0; i < lines; i++) { //this cycle shifts the non-zero colums on the left
            System.arraycopy(arrIn[i], s, arrIn[i], s - 1, cols - s);
            arrIn[i][cols - 1] = 0; //fill zeroes last col
        }
        for (int i = k; i < lines; i++) { //this cycle shifts the non-zero lines on the up
            System.arraycopy(arrIn[i], 0, arrIn[i - 1], 0, cols - 1);
            arrIn[i - 1] = arrIn[i].clone();
        }
        arrIn[lines - 1] = new int[cols]; //fill zeroes last line
        return arrIn;
    }
}