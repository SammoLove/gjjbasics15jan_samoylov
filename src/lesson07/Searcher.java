package lesson07;

public class Searcher {
    public static void searchStr(Employee[] where, String whatIsSearch) {
        int l = where.length;
        System.out.println("Search results by: " + whatIsSearch);
        for (int i = 0; i < l; i++) {
            if ((where[i].getName().toLowerCase().contains(whatIsSearch.toLowerCase())) ||
                    (where[i].getSurname().toLowerCase().contains(whatIsSearch.toLowerCase())) ||
                    (where[i].getMiddleName().toLowerCase().contains(whatIsSearch.toLowerCase()))) {
                System.out.println(i + ". " + where[i].getName() + " " + where[i].getSurname() + " " + where[i].getMiddleName());
            }
        }
    }
}
