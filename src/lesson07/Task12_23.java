package lesson07;

import test.Assert;

public class Task12_23 {
    public static void main(String[] args) {
        int[][] matrixATarget = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}};
        int[][] matrixBTarget = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}};
        int[][] matrixCTarget = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}};
        Assert.assertEquals(matrixATarget, fillA());
        Assert.assertEquals(matrixBTarget, fillB());
        Assert.assertEquals(matrixCTarget, fillC());
    }

    public static int[][] fillA() {
        int sizeXY = 7;
        int[][] arr = new int[sizeXY][sizeXY];
        for (int i = 0; i < sizeXY; i++) {
            for (int j = 0; j < sizeXY; j++) {
                if (i == j || i == sizeXY - j - 1) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] fillB() {
        int sizeXY = 7;
        int halfSizeXY = sizeXY / 2;
        int[][] arr = new int[sizeXY][sizeXY];
        for (int i = 0; i < sizeXY; i++) {
            for (int j = 0; j < sizeXY; j++) {
                if (i == j || i == sizeXY - j - 1 || i == halfSizeXY || j == halfSizeXY) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] fillC() {
        int sizeXY = 7;

        int[][] arr = new int[sizeXY][sizeXY];
        for (int i = 0; i < sizeXY; i++) {
            for (int j = 0; j < sizeXY; j++) {
                if ((j >= i && j <= 6 - i) || (j <= i && j >= sizeXY - i - 1)) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }
}