package lesson07;

import test.Assert;

public class Task11_158 {
    public static void main(String[] args) {
        int[] arrInput = {1, 2, 2, 3, 1, 4, 2, 5, 3, 5};
        int[] arrTarget = {1, 2, 3, 4, 5, 0, 0, 0, 0, 0};
        Assert.assertEquals(arrTarget, delRepeats(arrInput));
    }

    public static int[] delRepeats(int[] arr) {
        int l = arr.length;
        int[] arr1 = arr.clone(); //creating of copy to avoid modifying the input array
        for (int i = 0; i < l; i++) { //fixed new solutions with using of System.arraycopy
            for (int j = i + 1; j < l; j++) {
                if ((arr1[i] == arr1[j])) {
                    System.arraycopy(arr1, j + 1, arr1, j, l - j - 1);
                    arr1[l - 1] = 0;
                }
            }
        }
        return arr1;
    }
}