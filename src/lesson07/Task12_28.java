package lesson07;

import test.Assert;

public class Task12_28 {
    public static void main(String[] args) {
        int n = 5;
        int[][] arrTarget = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}};
        Assert.assertEquals(arrTarget, fillSpiral(n));
    }

    public static int[][] fillSpiral(int n) {
        int[][] arr = new int[n][n];
        int round = 0;
        int step = 1;
        int i;
        do { //one cycle - one round
            for (i = round; (i < n - round) && (step != n * n); i++) {
                arr[round][i] = step++;
            }
            for (i = round + 1; i < n - round - 1; i++) {
                arr[i][n - round - 1] = step++;
            }
            for (i = n - round - 1; i >= round; i--) {
                arr[n - round - 1][i] = step++;
            }
            for (i = n - round - 2; i > round; i--) {
                arr[i][round] = step++;
            }
            round++;
        } while (step <= n * n);
        return arr;
    }
}