package lesson07;

import java.util.Arrays;

public class Task12_25 {
    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(fillA()));
        System.out.println(Arrays.deepToString(fillB()));
        System.out.println(Arrays.deepToString(fillC()));
        System.out.println(Arrays.deepToString(fillD()));
        System.out.println(Arrays.deepToString(fillE()));
        System.out.println(Arrays.deepToString(fillF()));
    }

    public static int[][] fillA() {
        int sizeX = 12;
        int sizeY = 10;
        int[][] arr = new int[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                arr[i][j] = i * sizeY + j + 1;
            }
        }
        return arr;
    }

    public static int[][] fillB() {
        int sizeX = 12;
        int sizeY = 10;
        int[][] arr = new int[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                arr[i][j] = i + j * sizeX + 1;
            }
        }
        return arr;
    }

    public static int[][] fillC() {
        int sizeX = 12;
        int sizeY = 10;
        int[][] arr = new int[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                arr[i][j] = sizeY - j + i * sizeY;
            }
        }
        return arr;
    }

    public static int[][] fillD() {
        int sizeX = 12;
        int sizeY = 10;
        int[][] arr = new int[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                arr[i][j] = sizeX - i + j * sizeX;
            }
        }
        return arr;
    }

    public static int[][] fillE() {
        int sizeX = 10;
        int sizeY = 12;
        int[][] arr = new int[sizeX][sizeY];
        for (int i = 0, count = 1; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++, count++) {
                if ((i & 1) != 1) {
                    arr[i][j] = count;
                } else {
                    arr[i][sizeY - j - 1] = count;
                }
            }
        }
        return arr;
    }

    public static int[][] fillF() {
        int sizeX = 12;
        int sizeY = 10;
        int[][] arr = new int[sizeX][sizeY];
        for (int j = 0, count = 1; j < sizeY; j++) {
            for (int i = 0; i < sizeX; i++, count++) {
                if ((j & 1) != 1) {
                    arr[i][j] = count;
                } else {
                    arr[sizeX - i - 1][j] = count;
                }
            }
        }
        return arr;
    }
}