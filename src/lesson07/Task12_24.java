package lesson07;

import test.Assert;

public class Task12_24 {
    public static void main(String[] args) {
        int[][] matrixATarget = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}};
        int[][] matrixBTarget = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},};
        Assert.assertEquals(matrixATarget, fillA());
        Assert.assertEquals(matrixBTarget, fillB());
    }

    public static int[][] fillA() {
        int sizeXY = 6;
        int[][] arr = new int[sizeXY][sizeXY];
        for (int i = 0; i < sizeXY; i++) {
            for (int j = 0; j < sizeXY; j++) {
                if (i > 0 && j > 0) {
                    arr[i][j] = arr[i][j - 1] + arr[i - 1][j];
                } else {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] fillB() {
        int sizeXY = 6;
        int[][] arr = new int[sizeXY][sizeXY];
        for (int i = 0; i < sizeXY; i++) {
            for (int j = 0; j < sizeXY; j++) {
                arr[i][j] = (i + j) % sizeXY + 1;
            }
        }
        return arr;
    }
}