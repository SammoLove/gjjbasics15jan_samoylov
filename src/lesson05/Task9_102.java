package lesson05;

import test.Assert;

public class Task9_102 {
    public static void main(String[] args) {
        String str = "вперёд";
        StringBuilder strB = new StringBuilder(str);
        byte m = 2;
        byte n = 4;

        strB.setCharAt(n - 1, str.charAt(m - 1));
        strB.setCharAt(m - 1, str.charAt(n - 1));

        Assert.assertEquals("врепёд", strB.toString()); //test
    }
}