package lesson05;

import test.Assert;

public class Task9_166 {
    public static void main(String[] args) {
        String str = "Поменять местами его первое и последнее слово";
        String[] words = str.split("\\s");
        String temp = words[0];
        words[0] = words[words.length - 1];
        words[words.length - 1] = temp;

        str = "";
        for (String word : words) {
            str = str + word + " ";
        }

        System.out.println(str);
        Assert.assertEquals("слово местами его первое и последнее Поменять ", str);
    }
}
