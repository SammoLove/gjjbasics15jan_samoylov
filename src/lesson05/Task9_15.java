package lesson05;

import test.Assert;

public class Task9_15 {
    public static void main(String[] args) {
        String str = "Слово";
        char ch;
        byte k = 4;
        ch = str.charAt(k - 1);
        System.out.println(k + "th symbol of word " + str + " is " + ch);
        Assert.assertEquals('в', ch); //test
    }
}
