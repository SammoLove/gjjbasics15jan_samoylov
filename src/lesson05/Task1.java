package lesson05;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class Task1 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //java.nio.charset.Charset.defaultCharset();
        //System.out.println("(Кодировка: " + System.getProperty("file.encoding") + ")"); //just info
        //String s = new String(args[0].getBytes("866"));
        PrintStream ps = new PrintStream(System.out, true, "866");
        ps.println("Привет, " + args[0] + "!");
    }
}