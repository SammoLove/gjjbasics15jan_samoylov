package lesson05;

import test.Assert;

public class Task9_17 {
    public static void main(String[] args) {
        String str = "олово";
        boolean ifSame = false; //for tests

        if (str.charAt(0) == str.charAt(str.length() - 1)) {
            System.out.println("Word " + str + " begins and ends to the same letter");
            ifSame = true;
        } else {
            System.out.println("Word " + str + " begins and ends NOT to the same letter");
        }

        Assert.assertEquals(true, ifSame);//test
    }
}