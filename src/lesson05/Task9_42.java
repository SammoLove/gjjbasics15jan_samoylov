package lesson05;

import test.Assert;

public class Task9_42 {
    public static void main(String[] args) {
        String str = "вперёд";
        StringBuilder antiStr = new StringBuilder(str);

        for (byte i = 0; i < str.length(); i++) {
            antiStr.append(str.charAt(i));
        }

        Assert.assertEquals("врепёд", antiStr.toString()); //test
    }
}