package lesson11;

public class Sorting {

    public static void quickSort(int[] a) throws Exception {
        quickSort(a, 0, a.length - 1);
    }

    private static void swap(int[] array, int a, int b) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

    private static void quickSort(int a[], int loFrom, int hiTo) throws Exception {
        int lo = loFrom;
        int hi = hiTo;
        if (lo >= hi) { // exit condition of the recursion
            return;
        } else if (lo == hi - 1) {
            if (a[lo] > a[hi]) {
                swap(a, lo, hi);
            }
            return;
        }

        int middle = (int) (((long) lo + (long) hi) >> 1); //overflow possible ?
        int pivot = a[middle]; // pivot from the array middle
        a[middle] = a[hi];
        a[hi] = pivot;

        while (lo < hi) {
            while (a[lo] <= pivot && lo < hi) {
                lo++;
            } // search forward from a[lo]
            while (pivot <= a[hi] && lo < hi) {
                hi--;
            } // search backward from a[hi]
            if (lo < hi) {
                swap(a, lo, hi);
            }
        }
        a[hiTo] = a[hi]; // put the median in the "center"
        a[hi] = pivot;

        quickSort(a, loFrom, lo - 1); //Recursive calls, elements a[loFrom] to a[lo-1] are less than or
        quickSort(a, hi + 1, hiTo); //equal to pivot, elements a[hi+1] to a[hiTo] are greater than pivot.
    }
    // =================================================================

    public static void mergeSort(int a[]) {
        mergeSort(a, 0, a.length - 1);
    }

    private static void mergeSort(int a[], int loFrom, int hiTo) {
        int lo = loFrom;
        if (lo >= hiTo) { // exit condition of the recursion
            return;
        }

        int middle = (int) (((long) lo + (long) hiTo) >> 1); // split
        mergeSort(a, lo, middle);
        mergeSort(a, middle + 1, hiTo);

        // merge two sorted lists
        int endLo = middle;
        int startHi = middle + 1;
        while ((lo <= endLo) && (startHi <= hiTo)) {
            if (a[lo] < a[startHi]) {
                lo++;
            } else { // a[lo] >= a[startHi]
                int temp = a[startHi];
                /*for (int k = startHi - 1; k >= lo; k--) {
                    a[k + 1] = a[k];
                } after, while optimizing, replacing of this cycle with arraycopy improved speed to 3.5 times! */
                System.arraycopy(a, lo, a, lo + 1, startHi - lo);
                a[lo] = temp;
                lo++;
                endLo++;
                startHi++;
            }
        }
    }
    // ====================================================

    public static void binarySort(int a[]) {
        for (int i = 0; i < a.length; i++) {
            for (int j = a.length - 1; j > i; j--) {
                if (a[j] < a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    /* returns -key, if nothing was found */
    public static int binarySearch(int a[], int key) {
        return binarySearch(a, key, 0, a.length - 1);
    }

    private static int binarySearch(int a[], int key, int lo, int hi) {
        if (lo > hi) {
            return -key;
        }

        int med = a[(lo + hi) >> 1];
        if (med < key) {
            return binarySearch(a, key, ((lo + hi) >> 1) + 1, hi);
        } else if (med > key) {
            return binarySearch(a, key, lo, ((lo + hi) >> 1) - 1);
        } else {
            return (lo + hi) >> 1;
        }
    }
}