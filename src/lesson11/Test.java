package lesson11;

import test.Assert;
import test.MyTimer;

import java.util.Arrays;
import java.util.Random;

public class Test {
    public static void main(String[] args) throws Exception {

        int size = 100_000;
        int[] quickSortArray = new int[size];
        int[] mergeSortArray = new int[size];
        int[] binarySortArr = new int[size];
        int[] jdkSortArray = new int[size];

        Random randomGenerator = new Random();
        for (int i = 0; i < size; i++) {
            quickSortArray[i] = randomGenerator.nextInt(size * 10);
        }
        System.arraycopy(quickSortArray, 0, mergeSortArray, 0, quickSortArray.length); // do same input for all for honest experiment
        System.arraycopy(quickSortArray, 0, binarySortArr, 0, quickSortArray.length);
        System.arraycopy(quickSortArray, 0, jdkSortArray, 0, quickSortArray.length);

        MyTimer myTimer = new MyTimer();
        myTimer.start();
        Arrays.sort(jdkSortArray);
        System.out.println("jdkSort (Dual-Pivot Quick sort): " + myTimer.getElapsedMs() + " ms. ");

        myTimer.start();
        Sorting.quickSort(quickSortArray);
        System.out.print("quickSort: " + myTimer.getElapsedMs() + " ms. ");
        Assert.assertEquals(jdkSortArray, quickSortArray);

        myTimer.start();
        Sorting.mergeSort(mergeSortArray);
        System.out.print("mergeSort: " + myTimer.getElapsedMs() + " ms. ");
        Assert.assertEquals(jdkSortArray, mergeSortArray);

        myTimer.start();
        Sorting.binarySort(binarySortArr);
        System.out.print("binarySort: " + myTimer.getElapsedMs() + " ms. ");
        Assert.assertEquals(jdkSortArray, binarySortArr);

        System.out.print("Binary search test: ");
        int toSearch = 999;
        int jdkFoundAtPosition = Arrays.binarySearch(jdkSortArray, toSearch);
        int myFoundAtPosition = Sorting.binarySearch(jdkSortArray, toSearch);

        if (jdkFoundAtPosition < 0 && myFoundAtPosition < 0) {
            System.out.print("Element " + toSearch + " not found. ");
            Assert.assertEquals(false, false);
        } else if ((jdkFoundAtPosition < 0 && myFoundAtPosition > 0) || (jdkFoundAtPosition > 0 && myFoundAtPosition < 0)) {
            System.out.print("Results do not match. ");
            Assert.assertEquals(true, false);
        } else {
            System.out.print("Element " + toSearch + " was found on " + jdkFoundAtPosition + " position by both methods. ");
            Assert.assertEquals(jdkFoundAtPosition, myFoundAtPosition);
        }
    }
}