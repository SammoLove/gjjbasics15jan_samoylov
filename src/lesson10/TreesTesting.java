package lesson10;

import test.Assert;

public class TreesTesting {
    public static void main(String[] args) {
        SelfBalancingTree mySbtr = new SelfBalancingTree();
        int[] arr = {5, 7, 2, 9, 1, 6, 3, 10};
        for (int i : arr) {
            mySbtr.add(i); // simple adding, balancing not needed
        }
        Assert.assertEquals("(5(2(1)(3))(7(6)(9(10))))", mySbtr.toString()); // all elements of the array contained in a tree

        mySbtr.remove(10); // simple remove, balancing not needed, "10" removed
        Assert.assertEquals("(5(2(1)(3))(7(6)(9)))", mySbtr.toString());

        mySbtr.remove(5); // root delete, balancing not needed
        Assert.assertEquals("(6(2(1)(3))(7(9)))", mySbtr.toString());

        mySbtr.add(7); // right-left case
        Assert.assertEquals("(6(2(1)(3))(7(7)(9)))", mySbtr.toString());

        mySbtr.add(11);
        mySbtr.add(12); // left case
        Assert.assertEquals("(6(2(1)(3))(7(7)(11(9)(12))))", mySbtr.toString());

        mySbtr.add(19);
        mySbtr.add(18);
        mySbtr.add(16);
        mySbtr.add(15);
        mySbtr.add(14); // right case
        Assert.assertEquals("(11(6(2(1)(3))(7(7)(9)))(15(12(14))(18(16)(19))))", mySbtr.toString());

        mySbtr.add(2);
        mySbtr.remove(6);
        mySbtr.remove(7); // left-right case
        Assert.assertEquals("(11(3(2(1)(2))(7(9)))(15(12(14))(18(16)(19))))", mySbtr.toString());
    }
}