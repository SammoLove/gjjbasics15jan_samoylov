package lesson10;

public class SelfBalancingTree {
    private Node root;

    public void add(Object data) {
        root = add(root, data);
    }

    private Node add(Node newNd, Object data) {
        if (newNd == null) {
            if (this.root == null) {
                root = new Node(data);
            }
            newNd = new Node(data);
            return newNd;
        } else if (((Comparable) data).compareTo(newNd.getKey()) == -1) { //TODO add checking and compare to non-comparable types with generics
            newNd.setLeft(add(newNd.getLeft(), data));
        } else {
            newNd.setRight(add(newNd.getRight(), data));
        }
        return balance(newNd);
    }

    private byte getBalanceFactor(Node nd) {
        byte l = (nd != null && nd.getLeft() != null) ? nd.getLeft().getHeight() : 0;
        byte r = (nd != null && nd.getRight() != null) ? nd.getRight().getHeight() : 0;
        return (byte) (l - r);
    }

    private void fixHeight(Node nd) {
        byte l = nd.getLeft() != null ? nd.getLeft().getHeight() : 0; // -1?
        byte r = nd.getRight() != null ? nd.getRight().getHeight() : 0;
        byte newHeight = (byte) ((l > r ? l : r) + 1);
        nd.setHeight(newHeight);
    }

    private Node rightRotate(Node nd) {
        Node newRoot = nd.getLeft();
        nd.setLeft(newRoot.getRight());
        newRoot.setRight(nd);
        fixHeight(nd);
        fixHeight(newRoot);
        return newRoot;
    }

    private Node leftRotate(Node nd) {
        Node newRoot = nd.getRight();
        nd.setRight(newRoot.getLeft());
        newRoot.setLeft(nd);
        fixHeight(nd);
        fixHeight(newRoot);
        return newRoot;
    }

    private Node balance(Node nd) {
        fixHeight(nd);
        byte bf = getBalanceFactor(nd);
        if (bf == -2) {
            if (getBalanceFactor(nd.getRight()) > 0) {
                nd.setRight(rightRotate(nd.getRight()));
            }
            return leftRotate(nd);
        }
        if (bf == 2) {
            if (getBalanceFactor(nd.getLeft()) < 0) {
                nd.setLeft(leftRotate(nd.getLeft()));
            }
            return rightRotate(nd);
        }
        return nd;
    }

    private Node searchMin(Node nd) {
        return nd.getLeft() != null ? searchMin(nd.getLeft()) : nd;
    }

    private Node removeMin(Node nd) {
        if (nd.getLeft() == null) {
            return nd.getRight();
        }
        nd.setLeft(removeMin(nd.getLeft()));
        return balance(nd);
    }

    public void remove(Object data) {
        root = remove(root, data);
    }

    private Node remove(Node nd, Object data) {
        if (nd == null) {
            return null;
        }
        if (((Comparable) data).compareTo(nd.getKey()) == -1) { //TODO add checking and compare to non-comparable types with generics
            nd.setLeft(remove(nd.getLeft(), data));
        } else if (((Comparable) data).compareTo(nd.getKey()) == 1) {
            nd.setRight(remove(nd.getRight(), data));
        } else { //data == nd.key!
            Node ndl = nd.getLeft();
            Node ndr = nd.getRight();
            if (ndr == null) {
                return ndl;
            }
            Node min = searchMin(ndr);
            min.setRight(removeMin(ndr));
            min.setLeft(ndl);
            return balance(min);
        }
        return balance(nd);
    }

    @Override
    public String toString() {
        return toString(root, "");
    }

    private String toString(Node nd, String out) { //
        if (nd != null) {
            out += "(" + nd.getKey();
            out = toString(nd.getLeft(), out + "");
            out = toString(nd.getRight(), out + "");
            out += ")";
        }
        return out;
    }

    private class Node {
        private Object key;
        private byte height;
        private Node left;
        private Node right;

        public Node(Object key) {
            this.key = key;
            height = 1;
        }

        public byte getHeight() {
            return height;
        }

        public void setHeight(byte height) {
            this.height = height;
        }

        public Object getKey() {
            return key;
        }

        public Node getLeft() {
            return left;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public Node getRight() {
            return right;
        }

        public void setRight(Node right) {
            this.right = right;
        }
    }
}