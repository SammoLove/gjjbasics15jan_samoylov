package lesson10;

import test.MyTimer;

import java.util.Random;
import java.util.TreeSet;

public class TreesTest {
    public static void main(String[] args) {
        int limon = 1000_000;
        int[] randArr = new int[limon];
        TreeSet jdkTrst = null;
        MyTimer myTimer = null;
        SelfBalancingTree mySbtr = null;
        int retries = 4;

        Random r = new Random();
        for (int i = 0; i < limon; i++) {
            randArr[i] = r.nextInt(limon);
        }

        System.out.println("Test 1. Adding of 1'000'000 random elements to tree.");
        System.out.print("JDK TreeSet. ");
        for (int j = 1; j <= retries; j++) {
            jdkTrst = new TreeSet();
            myTimer = new MyTimer();
            myTimer.start();
            for (int i : randArr) {
                jdkTrst.add(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        System.out.print("My SelfBTree. ");
        for (int j = 1; j <= retries; j++) {

            mySbtr = new SelfBalancingTree();
            myTimer.start();
            for (int i : randArr) {
                mySbtr.add(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();
        System.out.println();

        //================================

        limon = (limon >> 1); // now 500_000
        for (int i = 0; i < limon; i++) { // new random set
            randArr[i] = r.nextInt(limon * 2);
        }

        System.out.println("Test 2. Removing of half elements from tree.");
        System.out.print("JDK TreeSet. ");
        for (int j = 1; j <= retries; j++) {
            myTimer.start();
            for (int i : randArr) {
                jdkTrst.remove(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        System.out.print("My SelfBTree. ");
        for (int j = 1; j <= retries; j++) {
            myTimer.start();
            for (int i : randArr) {
                mySbtr.remove(i);
            }
            System.out.print("Attempt " + j + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();
    }
}