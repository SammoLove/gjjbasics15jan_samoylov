package lesson08;

import com.sun.istack.internal.Nullable;

public class DoublyLinkedList {

    private int size;
    private Node node;
    private Node headAndTail;

    //2.1) реализовать метод int add(int e), с помощью которого можно добавлять элементы в конец массива. если свободное место закончилось, то увеличить размер массива. метод возвращает индекс вставленного элемента
    public int add(Object data) {
        if (headAndTail != null) { //elements already exist
            Node newNode = new Node(data, headAndTail.getPrev(), null);
            headAndTail.getPrev().setNext(newNode);
            headAndTail.setPrev(newNode);
        } else { //no one elements exist. headAndTail == null, add first
            node = new Node(data);
            headAndTail = new Node(null, node, node);
        }
        size++;
        return size-1;
    }

    //2.2) void add(int i, int e) для вставки элемента по индексу i со смещением остальных элементов
    public void add(int index, Object data) {
        if (headAndTail != null) { //if list is not empty
            Node current = iterateTo(index); // null if index out of bounds

            if (current.getPrev() == null) { // if it's first
                Node newNode = new Node(data, null, current);
                current.setPrev(newNode);
                headAndTail.setNext(newNode);
            } else if (current.getNext() == null) { // if it's last
                Node newNode = new Node(data, headAndTail.getPrev(), null);
                headAndTail.getPrev().setNext(newNode);
                headAndTail.setPrev(newNode);
            } else { // if it's not first and last
                Node newNode = new Node(data, current.getPrev(), current);
                current.getPrev().setNext(newNode);
                current.setPrev(newNode);
            }
        } else if (index == 0) { // data insertion in empty list
            node = new Node(data);
            headAndTail = new Node(null, node, node);
        } else {
            // exception "index out of bounds", nowhere to insert
        }
        size++;
    }

    private Node iterateTo(int index) {
        if (index < 0 || index >= size) {
            //return exception "out of bounds"
        }
        Node current = headAndTail;
        if (index < (size >> 1)) {
            for (int i = 0; i <= index; i++) { //&& current.getNext() != null redundant condition check
                current = current.getNext();
            }
        } else {
            for (int i = size - 1; i >= index; i--) { //&& current.getPrev() != null redundant condition check
                current = current.getPrev();
            }
        }
        return current;
    }

    //3) реализовать метод int get(int index), который возвращает элемент массива по индексу.
    public Object get(int index) {
        Node current = iterateTo(index);
        return (current.getData() != null) ? current.getData() : null;
    }

    private void removeNode(Node node) {
        if (node.getPrev() != null) { // if it's not first
            node.getPrev().setNext(node.getNext());
        } else {
            headAndTail.setNext(node.getNext());
        }
        if (node.getNext() != null) { // if it's not last
            node.getNext().setPrev(node.getPrev());
        } else {
            headAndTail.setPrev(node.getPrev());
        }
        size--;
    }

    //4) реализовать метод int remove(int index), который удаляет элемент по индексу из массива и возвращает удаленный элемент. оптимизировать удаление последнего элемента, не изменяя размер массива.
    public Object remove(int index) {
        Node current = iterateTo(index);
        Object tmp = current.getData();
        removeNode(current);
        return tmp;
    }

    //5) реализовать метод int removeElement(int e), который удаляет 1й найденный элемент и возвращает его индекс.
    public int removeElement(Object data) {
        int index = -1;
        Node current = headAndTail;
        if (headAndTail != null) { //if list is not empty
            while (current.getNext() != null & index++ != size) {
                if (current.getNext().getData() != null && current.getNext().getData().equals(data)) {
                    //found!
                    removeNode(current.getNext());
                    return index;
                }
                current = current.getNext();
            }
        } else {
            //return exception "empty list"
            return -1;
        }
        return -1;
    }

    //6) реализовать метод int size(), возвращающий количество элементов
    public int size() {
        return size;
    }

    //7) реализовать метод int indexOf(int e), возвращающий индекс элемента
    public int indexOf(Object data) {
        int index = -1;
        Node current = headAndTail;
        if (headAndTail != null) { //if list is not empty
            while (current.getNext() != null & index++ != size) {
                if (current.getNext().getData() != null && current.getNext().getData().equals(data)) {
                    return index; //found!
                }
                current = current.getNext();
            }
        } else {
            //return exception "empty list"
            return -1;
        }
        return -1;
    }

    //8) boolean contains(int e)
    public boolean contains(Object data) {
        return (indexOf(data) != -1);
    }

    //displays all elements
    @Override
    public String toString() {
        String print = "";
        for (int i = 0; i < size; i++) {
            print = print + get(i) + ", ";
        }
        print = print.substring(0, print.length() - 2);
        return print;
    }

    //9) int set(int i, int e), который устанавливает значение e по индексу i и возвращает предыдущее значение обновленного элемента
    public Object set(int index, Object data) {
        Object was = new Object();
        if (headAndTail != null) { //if list is not empty
            Node current = iterateTo(index);
            was = current.getData();
            current.setData(data);
        } else {
            // exception "list is empty, nothing to change!"
        }
        return was;
    }

    private class Node {
        private Object data;
        private Node prev;
        private Node next;

        public Node(Object data) {
            this.data = data;
        }

        public Node(Object data, Node prev, Node next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        @Nullable
        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        @Nullable
        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Nullable
        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node prev) {
            this.prev = prev;
        }
    }
}