package lesson08;

import test.Assert;

public class TaskDA {
    public static void main(String[] args) {
        DynamicArray arr1 = new DynamicArray();
        DynamicArray arr2 = new DynamicArray(9);

        // input 1, 2, 3...
        for (int i = 0; i < 7; i++) {
            arr1.add(i+1);
        }
        // display
        for (int i = 0; i < arr1.getSize(); i++) {
            System.out.print(arr1.get(i) + " ");
        }

        arr1.add(6, 13);
        System.out.println();
        Assert.assertEquals(8, arr1.add(20));
        Assert.assertEquals(true, arr1.contains(4));
        Assert.assertEquals(false, arr1.contains(40));
        arr1.add(1, 11);
        Assert.assertEquals(1, arr1.indexOf(11));
        Assert.assertEquals(3, arr1.remove(3));
        Assert.assertEquals(5, arr1.removeElement(6));
        Assert.assertEquals(1, arr1.set(0, -2));
        Assert.assertEquals(8, arr1.getSize());

        // display result
        for (int i = 0; i < arr1.getSize(); i++) {
            System.out.print(arr1.get(i) + " ");
        }
    }
}