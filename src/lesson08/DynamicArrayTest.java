package lesson08;

import test.Assert;

import java.util.ArrayList;

public class DynamicArrayTest {
    public static void main(String[] args) {
        int tenLimon = 10_000_000;
        int oneTh = 1_000;
        int retries = 2;
        //DynamicArray daMy = null;
        ArrayList<Object> daJdk = null;

/*
        System.out.println("Test 1. Creating of null filled list of 10 000 000 elements to DA.");
        MyTimer myTimer = new MyTimer();

        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            daMy = new DynamicArray(tenLimon);
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            daJdk = new ArrayList<Object>(tenLimon);
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 2. Adding of 10 000 000 elements to empty DA.");
        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            daMy = new DynamicArray();
            for (int j = 0; j < tenLimon; j++) {
                daMy.add(j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            daJdk = new ArrayList<Object>();
            for (int j = 0; j < tenLimon; j++) {
                daJdk.add(j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 3. Adding by index of 10 000 000 elements to non-empty DA.");
        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            daMy = new DynamicArray(oneTh);
            myTimer.start();
            for (int j = 0; j < tenLimon; j++) {
                daMy.add(j, j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            daJdk = new ArrayList<Object>(oneTh);
            myTimer.start();
            for (int j = 0; j < tenLimon; j++) {
                daJdk.add(j, j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 4. Getting by index of 10 000 000 elements to non-empty DA.");

        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < tenLimon; j++) {
                daMy.get(j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < tenLimon; j++) {
                daJdk.get(j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 5. Setting by index of 10 000 000 elements to non-empty DA.");

        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < tenLimon; j++) {
                daMy.set(j, j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < tenLimon; j++) {
                daJdk.set(j, j);
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 6. Removing by index of 1 000 elements from DA.");
        tenLimon--;
        Random r = new Random();

        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < oneTh; j++) {
                daMy.remove(r.nextInt(tenLimon));
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
            tenLimon -= oneTh;
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < oneTh; j++) {
                daJdk.remove(r.nextInt(tenLimon));
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 7. Removing by object of 1 000 elements form DA.");
        tenLimon -= oneTh;

        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < oneTh; j++) {
                daMy.removeElement(r.nextInt(tenLimon));
            }
            System.out.print("My. Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
            tenLimon -= oneTh;
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < oneTh; j++) {
                daJdk.remove((Object) r.nextInt(tenLimon));
            }
            System.out.print("Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }
        System.out.println();

        //=================================================================================
        System.out.println("Test 8. Searching by object of 1 000 elements in DA.");
        tenLimon -= oneTh;

        System.out.print("My. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < oneTh; j++) {
                daMy.indexOf(r.nextInt(tenLimon));
            }
            System.out.print("My. Attempt " + i + " took " + myTimer.getElapsedMs() + " ms. ");
        }

        System.out.print("JDK. ");
        for (int i = 1; i <= retries; i++) {
            myTimer.start();
            for (int j = 0; j < oneTh; j++) {
                daJdk.indexOf(r.nextInt(tenLimon));
            }
            System.out.print("Attempt " + i + " took  " + myTimer.getElapsedMs() + " ms. ");
        }
*/

        //=================================================================================
        // add test to compare removal of last element and optimize implementation
        DynamicArray daMy = new DynamicArray();
        daMy.add(1);
        daMy.add(3);
        daMy.add(5);
        daMy.add(7);
        daMy.removeElement(7); //removing last
        Assert.assertEquals(false, daMy.contains(7)); //now 7 is not contained in array, so deleting of last succesful
        daMy.remove(2); //removing last
        Assert.assertEquals(false, daMy.contains(5)); //now 5 is not contained in array, so deleting of last succesful
    }
}
