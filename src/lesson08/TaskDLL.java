package lesson08;

import test.Assert;

public class TaskDLL {
    public static void main(String[] args) {
        DoublyLinkedList myDLL = new DoublyLinkedList();

        boolean existed = myDLL.contains(123);
        myDLL.add(123);
        boolean exists = myDLL.contains(123);
        Assert.assertEquals(!existed, exists);

        myDLL.add(1, 11);
        myDLL.add(0, null);
        myDLL.add("I'm new");
        myDLL.add(2, "22");
        myDLL.add(null);
        myDLL.add("I'm last");

        Assert.assertEquals("null, 123, 22, 11, I'm new, null, I'm last", myDLL.toString());

        Assert.assertEquals(7, myDLL.size());
        Assert.assertEquals(null, myDLL.get(0));
        Assert.assertEquals("22", myDLL.get(2));
        Assert.assertEquals("I'm last", myDLL.remove(myDLL.size() - 1));
        Assert.assertEquals(4, myDLL.removeElement("I'm new"));
        Assert.assertEquals(123, myDLL.set(1, "1"));
        Assert.assertEquals(5, myDLL.size());
        Assert.assertEquals(2, myDLL.indexOf("22"));
        Assert.assertEquals(true, myDLL.contains("22"));
        Assert.assertEquals(false, myDLL.contains("99"));

        Assert.assertEquals("null, 1, 22, 11, null", myDLL.toString());
    }
}
