package lesson08;

public class DynamicArray {
    private Object[] array;
    private int size;
    private int last;

    //также предусмотреть создание массива без задания начального размера (выбрать дефолтный размер самим)
    public DynamicArray () {
        this(10);
    }

    //1) можно задать размер массива при его создании.
    public DynamicArray (int size){
        this.array = new Object[size];
        this.size = size;
        this.last = 0;
    }

    private void incSize () {
        if (last == size) {
            size *= 2;
            Object[] tmp = new Object[size];
            System.arraycopy(array, 0, tmp, 0, size / 2);
            //array = tmp.clone();
            array = tmp;
        }
    }

    //2.1) реализовать метод int add(int e), с помощью которого можно добавлять элементы в конец массива. если свободное место закончилось, то увеличить размер массива. метод возвращает индекс вставленного элемента
    public int add (Object e) {
        incSize();
        array[last++] = e;
        return last -1;
    }

    //2.2) void add(int i, int e) для вставки элемента по индексу i со смещением остальных элементов
    public void add(int index, Object e) {
        incSize();
        if (index >= size - 1 || index<0) {
            //exeption
        }
        System.arraycopy(array, index, array, index+1, last -index);
        array[index] = e;
        last++;
    }

    //3) реализовать метод int get(int index), который возвращает элемент массива по индексу.
    public Object get(int index) {
        return array[index];
    }

    //4) реализовать метод int remove(int index), который удаляет элемент по индексу из массива и возвращает удаленный элемент. оптимизировать удаление последнего элемента, не изменяя размер массива.
    public Object remove(int index) {
        Object tmp = array[index];
        if (index > size - 1 || index < 0) {
            // return exception;
            return -1;
        }
        if (index != last - 1) {
            System.arraycopy(array, index + 1, array, index, last - index - 1);
        }
        last--;
        array[last] = null; //was 0
        return tmp;
    }

    //5) реализовать метод int removeElement(int e), который удаляет 1й найденный элемент и возвращает его индекс.
    public int removeElement(Object e) {
        int toDel = indexOf(e);
        if  (toDel != -1) {
            remove(toDel);
            return toDel;
        }
        return -1; // -1 means not found
    }

    //6) реализовать метод int size(), возвращающий количество элементов
    public int getSize() {
        return last;
    }

    //7) реализовать метод int indexOf(int e), возвращающий индекс элемента
    public int indexOf(Object e) {
        for (int i = 0; i < last; i++) {
            if (array[i] != null && array[i].equals(e)) {
                return i;
            }
        }
        return -1; // -1 means not found
    }

    //8) boolean contains(int e)
    public boolean contains(Object e) {
        return indexOf(e) != -1;
    }

    //9) int set(int i, int e), который устанавливает значение e по индексу i и возвращает предыдущее значение обновленного элемента
    public Object set(int index, Object e) {
        if (index >= size - 1 || index<0) {
            // return exeption;
            return -1;
        }
        Object tmp = array[index];
        array[index] = e;
        return tmp;
    }
}