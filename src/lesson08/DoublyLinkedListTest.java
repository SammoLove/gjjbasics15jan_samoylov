package lesson08;

import test.MyTimer;

import java.util.LinkedList;

public class DoublyLinkedListTest {
    public static void main(String[] args) {
        int tenLimon = 10_000_000;
        int tenTh = 10_000;
        LinkedList<Object> dllJdk;
        DoublyLinkedList myDLL;

        MyTimer myTimer = new MyTimer();
        myTimer.start();
        dllJdk = new LinkedList<>();
        for (int i = 0; i < tenLimon; i++) { dllJdk.add(i); }
        System.out.println("Adding of 10 000 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        myDLL = new DoublyLinkedList();
        for (int i = 0; i < tenLimon; i++) { myDLL.add(i); }
        System.out.println("Adding of 10 000 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        dllJdk = new LinkedList<>();
        for (int i = 0; i < tenTh; i++) { dllJdk.add(i); }
        System.out.println("Adding of 10 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        myDLL = new DoublyLinkedList();
        for (int i = 0; i < tenTh; i++) { myDLL.add(i); }
        System.out.println("Adding of 10 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        //dllJdk = new LinkedList<>();
        for (int i = 0; i < tenTh; i++) { dllJdk.add(i, i); }
        System.out.println("Inserting of 10 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        //myDLL = new DoublyLinkedList();
        for (int i = 0; i < tenTh; i++) { myDLL.add(i, i); }
        System.out.println("Inserting of 10 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");


        myTimer.start();
        for (int i = 0; i < tenTh; i++) { dllJdk.set(i, i); }
        System.out.println("Setting of 10 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { myDLL.set(i, i); }
        System.out.println("Setting of 10 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");


        myTimer.start();
        for (int i = 0; i < tenTh; i++) { dllJdk.get(i); }
        System.out.println("Getting of 10 000 elements in jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { myDLL.get(i); }
        System.out.println("Getting of 10 000 elements in my DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { dllJdk.remove(i); }
        System.out.println("Removing by index from 10 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { myDLL.remove(i); }
        System.out.println("Removing by index from 10 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { dllJdk.remove((Object) i); }
        System.out.println("Removing by Object from 10 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { myDLL.removeElement(i); }
        System.out.println("Removing by Object from 10 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { dllJdk.indexOf(i); }
        System.out.println("Searching by Object in 10 000 elements to jdk DLL took " + myTimer.getElapsedMs() + " ms");

        myTimer.start();
        for (int i = 0; i < tenTh; i++) { myDLL.indexOf(i); }
        System.out.println("Searching by Object in 10 000 elements to my DLL took " + myTimer.getElapsedMs() + " ms");
    }
}