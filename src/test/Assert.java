package test;

import java.util.Arrays;

public class Assert {

    public static void assertEquals(double expected, double actual) {
        if (expected == actual) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(char expected, char actual) {
        if (expected == actual) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(boolean expected, boolean actual) {
        if (actual == expected) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(Object expected, Object actual) {
        if (expected != null && actual != null && expected.equals(actual)) {
            System.out.println("The test is successful :)");
        } else if (expected == null && actual == null) {
            System.out.println("The test is successful :) both null");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(int a, int aWas, int b, int bWas) {
        if (a == bWas && b == aWas) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :(");
        }
    }

    public static void assertEquals(int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(byte[] expected, byte[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(byte[][] expected, byte[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println("The test is successful :)");
        } else {
            System.out.println("The test is failure :( Expected: " + expected + ", Actual: " + actual);
        }
    }
}
