package test;

public class MyTimer {
    private static long time;
    public static void start() {
        time = System.nanoTime();
    }
    public static long getElapsedTime() {
        return (System.nanoTime() - time)/1000000;
    }
}