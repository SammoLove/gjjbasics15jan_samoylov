package lesson04;

import java.util.Scanner;

public class Task4_106 {
    public static void main(String[] args) {

        System.out.print("Enter year: ");
        Scanner scan = new Scanner(System.in);
        int year = scan.nextInt();
        System.out.println();
        System.out.print("This is the year of ");

        //оставляю только от 1 до 60
        int period60 = (year - 1984) % 60;
        //обрабатываю отрицательные
        if (period60 < 0) {
            period60 += 60;
        }

        int color = period60 / 12;
        switch (color) {
            case 0:
                System.out.print("green ");
                break;
            case 1:
                System.out.print("red ");
                break;
            case 2:
                System.out.print("yellow ");
                break;
            case 3:
                System.out.print("white ");
                break;
            case 4:
                System.out.print("black ");
                break;
        }

        int animal = period60 % 12;
        switch (animal) {
            case 0:
                System.out.print("rat");
                break;
            case 1:
                System.out.print("cow");
                break;
            case 2:
                System.out.print("tiger");
                break;
            case 3:
                System.out.print("hare");
                break;
            case 4:
                System.out.print("dragon");
                break;
            case 5:
                System.out.print("snake");
                break;
            case 6:
                System.out.print("horse");
                break;
            case 7:
                System.out.print("sheep");
                break;
            case 8:
                System.out.print("monkey");
                break;
            case 9:
                System.out.print("hen");
                break;
            case 10:
                System.out.print("dog");
                break;
            case 11:
                System.out.print("pig");
                break;
        }
    }
}