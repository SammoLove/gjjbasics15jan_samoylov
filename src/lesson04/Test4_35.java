package lesson04;

import test.Assert;

public class Test4_35 {
    public static void main(String[] args) {
        Assert.assertEquals("red", getColor(3));
        Assert.assertEquals("green", getColor(5));

        //additional tests
        Assert.assertEquals("green", getColor(3 + 2 + 2.5));
        Assert.assertEquals("red", getColor(3 + 2 + 3.5));
    }

    // returns color of lighting in current minute
    public static String getColor(double min) {
        String res = ((min % 5) < 3) ? "green" : "red";
        return res;
    }
}