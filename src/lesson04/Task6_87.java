package lesson04;

import java.util.Scanner;

public class Task6_87 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean endGame = false;
        byte team = 0;
        byte points = 0;
        byte team1Points = 0;
        byte team2Points = 0;
        System.out.println("For pointing end of game enter 0 team!");

        do {
            System.out.print("Number of team (1 or 2): ");
            team = scan.nextByte();
            if (team == 0) {
                endGame = true;
                break;
            }
            System.out.print("How many point (1,2 or 3): ");
            points = scan.nextByte();

            switch (team) {
                case 1:
                    team1Points += points;
                    break;
                case 2:
                    team2Points += points;
                    break;
            }
            System.out.println("Current score is " + team1Points + ":" + team2Points);
        } while (!endGame);

        if (team1Points > team2Points) {
            System.out.println("The winner is first team with a score of " + team1Points + ":" + team2Points);
        } else if (team1Points < team2Points) {
            System.out.println("The winner is second team with a score of " + team1Points + ":" + team2Points);
        } else {
            System.out.println("The game was ended in a draw with a score of " + team1Points + ":" + team2Points);
        }
    }
}