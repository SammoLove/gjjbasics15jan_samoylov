package lesson04;

import test.Assert;

public class Task4_15 {
    public static void main(String[] args) {
        Assert.assertEquals(29, whatAge(6, 1985, 12, 2014));
        Assert.assertEquals(28, whatAge(6, 1985, 5, 2014));
        Assert.assertEquals(29, whatAge(6, 1985, 6, 2014));
    }

    public static int whatAge(int bdMonth, int bdYear, int currentMonth, int currentYear) {
        int age = currentYear - bdYear;
        if (currentMonth < bdMonth) {
            age -= 1;
        }
        return age;
    }
}
