package lesson04;

import java.util.Scanner;

public class Task5_27 {
    public static void main(String[] args) {
        //a
        System.out.println("a) Sum from 100 to 500 = " + getSumAll(100, 500));

        System.out.println("Enter value <=500: ");
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        System.out.println("b) Sum from " + a + " to 500 = " + getSumAll(a, 500));
        System.out.println();

        System.out.println("Enter value >= -10: ");
        int b = scan.nextInt();
        System.out.println("c) Sum from -10 to " + b + " = " + getSumAll(-10, b));
        System.out.println();

        System.out.println("Enter value a (b>=a): ");
        a = scan.nextInt();
        System.out.println("Enter value b (b>=a): ");
        b = scan.nextInt();
        System.out.println("d) Sum from " + a + " to " + b + " = " + getSumAll(a, b));
        System.out.println();
    }

    // returns color of lighting in current minute
    public static int getSumAll(int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += i;
        }
        return sum;
    }
}