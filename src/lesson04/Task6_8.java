package lesson04;

import test.Assert;

public class Task6_8 {
    public static void main(String[] args) {
        int n = 10;
        int newVal = 0;
        int prevVal = 0;
        int i = 1;
        do {
            prevVal = newVal;
            newVal = i * i;
            i++;
        } while (newVal < n);

        System.out.println("Result: " + prevVal);

        Assert.assertEquals(9, prevVal);
    }
}
