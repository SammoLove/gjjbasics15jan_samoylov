package lesson04;

import test.Assert;

public class Task4_97 {
    public static void main(String[] args) {
        Assert.assertEquals("winter", getSeason(12));
    }

    // returns season by month
    public static String getSeason(int k) {
        String ses = "";
        switch (k) {
            case 12:
            case 1:
            case 2:
                ses = "winter";
                break;
            case 3:
            case 4:
            case 5:
                ses = "spring";
                break;
            case 6:
            case 7:
            case 8:
                ses = "summer";
                break;
            case 9:
            case 10:
            case 11:
                ses = "autumn";
                break;
        }
        System.out.println("There is " + ses + " now");
        return ses;
    }
}