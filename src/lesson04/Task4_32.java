package lesson04;

import test.Assert;

public class Task4_32 {
    public static void main(String[] args) {
        Assert.assertEquals(false, isEven(15));
        Assert.assertEquals(true, isEven(12));
    }

    // returns true if the last digit of the number is even
    public static boolean isEven(int a) {
        boolean res = ((a % 10) % 2 == 0) ? true : false;
        return res;
    }
}
