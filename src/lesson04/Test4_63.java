package lesson04;

import test.Assert;

public class Test4_63 {
    public static void main(String[] args) {
        Assert.assertEquals(false, isDayOff(5));
        Assert.assertEquals(true, isDayOff(7));
    }

    // returns true if k-day is day off
    public static boolean isDayOff(int k) {
        boolean res = false;
        if ((k % 6) == 0 | (k % 7) == 0) {
            res = true;
        }
        return res;
    }
}
