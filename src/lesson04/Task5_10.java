package lesson04;

import java.util.Scanner;

public class Task5_10 {
    public static void main(String[] args) {
        System.out.print("Enter exchange USD/RUR rate: ");
        Scanner scan = new Scanner(System.in);
        double exRate = scan.nextDouble();
        for (byte i = 1; i <= 20; i++) {
            System.out.println(i + "USD in RUR = " + i * exRate);
        }
    }
}