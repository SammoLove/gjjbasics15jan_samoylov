package lesson04;

import test.Assert;

public class Task5_64 {
    public static void main(String[] args) {
        double commonSqare = 0;
        final double POPULATION[] = {1, 9.5, 5.7, 10, 5, 2, 5.7, 11.1, 7, 0.8, 1.9, 2};
        final double DENSITY[] = {1, 16, 6, 1.5, 4, 7, 3.1, 3.1, 7.1, 2.8, 2.9, 2};

        for (int i = 0; i < 12; i++) {
            commonSqare += getSquare(POPULATION[i], DENSITY[i]);
        }

        /** evaluating without using arrays
         commonSqare += getSquare(1, 1);
         commonSqare += getSquare(9.5, 16);
         commonSqare += getSquare(5.7, 6);
         commonSqare += getSquare(10, 5.2);
         commonSqare += getSquare(5, 1.5);
         commonSqare += getSquare(2, 4);
         commonSqare += getSquare(5.7, 6);
         commonSqare += getSquare(11.1, 4.1);
         commonSqare += getSquare(7, 9);
         commonSqare += getSquare(0.8, 2.1);
         commonSqare += getSquare(1.9, 3);
         commonSqare += getSquare(2, 5.2);
         */

        System.out.println("The square or whole region is: " + commonSqare);

        //test of commongetSquare method
        Assert.assertEquals(10, getSquare(10, 1));
    }

    public static double getSquare(double population, double density) {
        return (population / density);
    }
}